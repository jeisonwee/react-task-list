import autor from './img/photo_profile.png';
import './App.css';
import ListaTarea from './componentes/ListaTarea.jsx';


function App() {
  return (
    <div className="app-tareas">
      <div className='cont_project'>
        <img 
          className='img_author' 
          src={autor} 
          alt='Logo de React' 
        />
        <h1>Task List with React</h1>
        <h6>by <span className='author_name'>Jeisson León</span></h6>
      </div>

      <div className='cont-tareas'>
        <h2>Lista de Tareas</h2>
        <div className='cont-list'>
          <ListaTarea />
        </div>
      </div>

      <div className='footer'>
        <p><b className='f_init'>&gt;</b> This was created with &lt;3 for <span className='f_author'>Jeisson León</span></p>
      </div>
    </div>
  );
}

export default App;
