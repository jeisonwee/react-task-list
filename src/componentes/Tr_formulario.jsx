import React, { useState } from 'react';
import '../css/Tr_formulario.css';
import { v4 as uuidv4 } from 'uuid';


function TrForm(props) {
  const [input, setInput] = useState('');

  const manejarCambio = e => {
    setInput(e.target.value);
    e.target.className = 'tr-input';
    e.target.placeholder = 'Escribe una tarea nueva';
  }

  const manejarEnvio = e => {
    e.preventDefault();
    
    const tareaNueva = {
      id: uuidv4(),
      texto: input,
      completada: false
    }
    props.onsubmit(tareaNueva);
    setInput('');
    
    let inputCont = document.querySelector('.tr-input');
    inputCont.value = '';
  }

  return (
    <form 
      className={`tr-form`}
      onSubmit={manejarEnvio}>
      <input 
        className={`tr-input ${props.esta ? 'act' : 'des'}`}
        type="text"
        placeholder={props.cont}
        onChange={manejarCambio} 
      />
      <button className='tr-btn'>
        Agregar
      </button>
    </form>
  );
}


export default TrForm;