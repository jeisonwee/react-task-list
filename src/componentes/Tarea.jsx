import React from 'react';
import '../css/Tarea.css';
import ico_del from '../img/icon-delete.svg';
import ico_ready from '../img/icon-ready.svg';


function Tarea({ id, texto, completada, completarTr, eliminarTr }) {
  return (
    <div className={completada ? 'tr-cont completada' : 'tr-cont'}>
      <div className='tr-text'>
        {texto}
      </div>
      <div className='tr-icon-cont'>
        <img 
          src={ico_ready} 
          className='tr-icon-delete' 
          onClick={() => completarTr(id)}
          alt="Check de realizado" 
        />
        <img 
          src={ico_del} 
          className='tr-icon-correct' 
          onClick={() => eliminarTr(id)}
          alt="Check de eliminacion" 
        />
      </div>
    </div>
  );
}

export default Tarea;