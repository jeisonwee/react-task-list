import React, { useState } from 'react';
import TrForm from './Tr_formulario';
import '../css/ListaTareas.css';
import Tarea from './Tarea';

function ListaTarea() {
  
  function getTaskList() { 
    var listActualizada = []; 
    var storedTask = localStorage.getItem('localTaskList');
    if( storedTask == null ) {
      listActualizada = [];
    } else {
      listActualizada = JSON.parse(storedTask);
    }
    return listActualizada;
  }

  var [tareas, setTarea] = useState(getTaskList());
  const [estado, setAlerta] = useState(false);
  const [mess, setMess] = useState('Escribe una tarea nueva');

  const agregarTr = tarea => {
    if(tarea.texto.trim()) {
      tarea.texto = tarea.texto.trim();
      const tareaActualizadas = [tarea, ...tareas];
      setTarea(tareaActualizadas);
      localStorageFriendList(tareaActualizadas);
    } else {
      setAlerta(true);
      setMess("Bro, ingresa algun valor :(");
    }
  }

  const completarTr = trId => {
    const tareasActualizadas = tareas.map(tarea => {
      if( tarea.id === trId ) {
        tarea.completada = !tarea.completada;
      }
      return tarea;
    })

    setTarea(tareasActualizadas);
    localStorageFriendList(tareasActualizadas);
  }

  const eliminarTr = tarea => {
    const tareaLimpiada = tareas.filter(item => item.id !== tarea)
    setTarea(tareaLimpiada);
    localStorageFriendList(tareaLimpiada);
  }

  /*--- Funcion de guardado local wey ---*/
  function localStorageFriendList(plist) {
    localStorage.setItem('localTaskList', JSON.stringify(plist));
  }

  return (
    <>
      <TrForm onsubmit={agregarTr} esta={estado} cont={mess} />
      <div className='ry-div' />
      <div className='tr-list-cont'>
      {
        tareas.map((tarea) =>
          <Tarea 
            key={tarea.id}
            id={tarea.id}
            texto={tarea.texto} 
            completada={tarea.completada}
            eliminarTr={eliminarTr}
            completarTr={completarTr}
          />
        )
      }
      </div>
    </>
  );
}

export default ListaTarea;